module Main where

import qualified Application (startApp)

main :: IO ()
main = do
  putStrLn "Running"
  Application.startApp
