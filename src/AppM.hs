{-# LANGUAGE NoFieldSelectors #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE RankNTypes #-}
module AppM where
import Servant.Auth.Server
    ( CookieSettings, JWTSettings, ToJWT, SetCookie )
import Servant (Handler, ServerError, AddHeader)
import Control.Monad.Reader (ReaderT)
import Effectful
import Effectful.Error.Static (Error, runErrorNoCallStack)
import qualified Control.Monad.Error.Class as T
import Data.Text (Text)
import qualified Data.ByteString as S
import Crypto.Hash.IO
import Crypto.Hash
import Data.Text.Encoding (encodeUtf8)
import Data.ByteArray (convert)
import Effectful.Dispatch.Static
import qualified Servant.Auth.Server as SAS
import Effectful.Dispatch.Dynamic
import User.Type
import Database.SQLite.Simple (withConnection, executeNamed, NamedParam (..), queryNamed)

data AppCtx = AppCtx
    {
        cookieSettings :: CookieSettings,
        jwtSettings :: JWTSettings,
        dbName :: String
    }

type AppM = ReaderT AppCtx Handler

data Login :: Effect where
    AcceptLogin
        :: ( ToJWT session
           , AddHeader "Set-Cookie" SetCookie response withOneCookies
           , AddHeader "Set-Cookie" SetCookie withOneCookies withTwoCookies
           )
        => CookieSettings -> JWTSettings -> session -> Login m (Maybe (response -> withTwoCookies))

type instance DispatchOf Login = Static WithSideEffects
newtype instance StaticRep Login = LoginImpl { acceptLoginImpl ::
       forall session response withOneCookies withTwoCookies. ( ToJWT session
       , AddHeader "Set-Cookie" SetCookie response withOneCookies
       , AddHeader "Set-Cookie" SetCookie withOneCookies withTwoCookies
       )
    => CookieSettings -> JWTSettings -> session -> IO (Maybe (response -> withTwoCookies)) }

acceptLogin
    :: ( Login :> es
       , ToJWT session
       , AddHeader "Set-Cookie" SetCookie response withOneCookies
       , AddHeader "Set-Cookie" SetCookie withOneCookies withTwoCookies
       )
    => CookieSettings -> JWTSettings -> session -> Eff es (Maybe (response -> withTwoCookies))
acceptLogin cookieSettings jwtSettings session = do
    LoginImpl acceptLoginImpl <- getStaticRep
    unsafeEff_ $ acceptLoginImpl cookieSettings jwtSettings session

runLogin :: (IOE :> es) => Eff (Login : es) a -> Eff es a
runLogin = evalStaticRep (LoginImpl SAS.acceptLogin)

data Hash :: Effect where
    HashText :: Text -> Hash m S.ByteString
type instance DispatchOf Hash = Dynamic

hashText :: (Hash :> es) => Text -> Eff es S.ByteString
hashText text = send (HashText text)

hashTextImpl :: Text -> IO S.ByteString
hashTextImpl password = do
    ctx <- hashMutableInitWith SHA3_512
    liftIO $ hashMutableUpdate ctx (encodeUtf8 password)
    convert @_ @S.ByteString <$> liftIO (hashMutableFinalize ctx)

runHash :: IOE :> es => Eff (Hash : es) a -> Eff es a
runHash = interpret \_ -> \case
    HashText text -> liftIO do
        ctx <- hashMutableInitWith SHA3_512
        hashMutableUpdate ctx (encodeUtf8 text)
        convert <$> hashMutableFinalize ctx

effToHandler :: Eff [Error ServerError, IOE] a -> Handler a
effToHandler m = do
    er <- liftIO . runEff . runErrorNoCallStack @ServerError $ m
    either T.throwError pure er

data UserDB :: Effect where
    InsertUser :: Username -> Email -> Password -> UserDB m ()
    ConfirmUserPassword :: Email -> Password -> UserDB m (Maybe User)
type instance DispatchOf UserDB = Dynamic

insertUser :: (UserDB :> es) => Username -> Email -> Password -> Eff es ()
insertUser username email password = send (InsertUser username email password)

confirmUserPassword :: (UserDB :> es) => Email -> Password -> Eff es (Maybe User)
confirmUserPassword email password = send (ConfirmUserPassword email password)

runSqliteSimple :: (IOE :> es, Hash :> es) => String -> Eff (UserDB : es) a -> Eff es a
runSqliteSimple db = interpret \_ -> \case
    InsertUser username email password -> do
        hashedPassword <- hashText password
        liftIO $ withConnection db \conn ->
            executeNamed conn
                "insert into user (username, email, password) values (:username, :email, :password)"
                [
                    ":username" := username,
                    ":email" := email,
                    ":password" := hashedPassword
                ]

    ConfirmUserPassword email password -> do
        hashed <- hashText password
        liftIO $ withConnection db \conn -> do
            result <- queryNamed conn "select email, username from user where email = :email and password = :password" [":email" := email, ":password" := hashed]
            case result of
                [] -> pure Nothing
                [user] -> pure $ Just user
                (_:_) -> pure Nothing