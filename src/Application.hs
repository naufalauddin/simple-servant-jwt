{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedRecordDot #-}
{-# LANGUAGE NoFieldSelectors #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE FlexibleContexts #-}
module Application (startApp) where

import User.Type
import AppM

import Servant hiding (throwError)
import Data.Aeson
import qualified Network.Wai.Handler.Warp as Warp
import Servant.Auth.Server hiding (acceptLogin)
import GHC.Generics
import Effectful (Eff, IOE)
import Effectful.Error.Static (Error, throwError)
import qualified Effectful as Eff
import Effectful.Reader.Static (runReader, asks, Reader)

data RootAPI auths mode =
    RootAPI
        {
            login :: mode :- "login" :> ReqBody '[JSON] LoginReq :> Post '[JSON] (Headers '[Header "Set-Cookie" SetCookie, Header "Set-Cookie" SetCookie] Value),
            signup :: mode :- "signup" :> ReqBody '[JSON] SignupReq :> Post '[JSON] Value,
            protected :: mode :- "self" :> Auth auths User :> Get '[JSON] User
        }
    deriving (Generic)

type API = NamedRoutes (RootAPI '[JWT, Cookie])

port :: Int
port = 8080

server
    :: ( Error ServerError Eff.:> es
       , Login Eff.:> es
       , Reader AppCtx Eff.:> es
       , IOE Eff.:> es
       , Hash Eff.:> es
       , UserDB Eff.:> es
       )
    => ServerT API (Eff es)
server = RootAPI
    {
        login = loginHandler,
        protected = protectedHandler,
        signup = signupHandler
    }

signupHandler :: (Reader AppCtx Eff.:> es, Hash Eff.:> es, UserDB Eff.:> es) => SignupReq -> Eff es Value
signupHandler req = do
    insertUser req.password req.email req.password 
    pure (toJSON req)

protectedHandler :: (Error ServerError Eff.:> es) => AuthResult User -> Eff es User
protectedHandler (Authenticated user) = pure user
protectedHandler _ = throwError err401 { errBody = "Not Authenticated" }

onNothing :: Applicative f => f a -> Maybe a -> f a
onNothing handleNothing Nothing = handleNothing
onNothing _ (Just a) = pure a

loginHandler :: (Error ServerError Eff.:> es, Reader AppCtx Eff.:> es, Login Eff.:> es, IOE Eff.:> es, Hash Eff.:> es, UserDB Eff.:> es) => LoginReq -> Eff es (Headers '[Header "Set-Cookie" SetCookie, Header "Set-Cookie" SetCookie] Value)
loginHandler req = do
    cookieSettings <- asks @AppCtx (.cookieSettings)
    jwtSettings <- asks @AppCtx (.jwtSettings)
    user <- confirmUserPassword req.email req.password
        >>= onNothing (throwError (err400 { errBody = "There is no user with such email and passwords" }))
    addCookie <- acceptLogin cookieSettings jwtSettings user
        >>= onNothing (throwError (err401 {errBody = "Login failed! Please try again"}))
    pure (addCookie $ object [])


app :: CookieSettings -> JWTSettings -> String -> Application
app cookieSettings jwtSettings dbName =
    let appCtx = AppCtx { cookieSettings, jwtSettings, dbName }
        interpret = effToHandler . runLogin . runHash . runSqliteSimple dbName . runReader appCtx
    in serveWithContext (Proxy @API) (cookieSettings :. jwtSettings :. EmptyContext) $
        hoistServerWithContext (Proxy @API) (Proxy @'[JWTSettings, CookieSettings]) interpret server

startApp :: IO ()
startApp = do
    jwtKey <- generateKey
    let jwtSettings = defaultJWTSettings jwtKey
        cookieSettings = defaultCookieSettings
        dbName = "jwtservant.sqlite"
    Warp.run port (app cookieSettings jwtSettings dbName)
