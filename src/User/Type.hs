{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE NoFieldSelectors #-}
module User.Type (LoginReq(..), User(..), SignupReq(..), Email, Username, Password) where
import Data.Aeson
import GHC.Generics
import Data.Text (Text)
import Database.SQLite.Simple (FromRow, ToRow)
import Servant.Auth.Server (FromJWT, ToJWT)

type Email = Text
type Username = Text
type Password = Text

data LoginReq = LoginReq { email :: Email, password :: Password }
    deriving stock (Show, Eq, Generic)
    deriving anyclass (FromJSON, ToJSON)
data User = User { email :: Email, username :: Username }
    deriving (Show, Eq, Generic)
    deriving anyclass (FromJSON, FromRow, ToJSON, FromJWT, ToJWT)
data SignupReq = SignupReq { email :: Email, username :: Username, password :: Password }
    deriving (Show, Eq, Generic)
    deriving anyclass (FromJSON, FromRow, ToRow, ToJSON, FromJWT, ToJWT)